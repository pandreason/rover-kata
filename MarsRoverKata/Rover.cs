﻿using System;

namespace MarsRoverKata
{
    /// <summary>
    /// Represents a rover capable of taking movement commands to traverse a grid
    /// </summary>
    /// <remarks>Authors: Geoff Mazeroff, Cameron Presley, Phillip Andreason</remarks>
    public class Rover
    {
        /// <summary>
        /// Prefix for the exception message invovling invalid movement commands
        /// </summary>
        public const string InvalidCommandPrefix = "Invalid command: ";

        /// <summary>
        /// Creates a Rover instance oriented in the given direction, starting at the given
        /// x-y coordinate, an engine, and a navigator
        /// </summary>
        /// <param name="direction">Starting direction</param>
        /// <param name="x">Starting x-coordinate</param>
        /// <param name="y">Starting y-coordinate</param>
        /// <param name="roverEngine">IRoverEngine to support moving the rover</param>
        /// <param name="navigator">INavigator to support navigating a grid</param>
        /// <exception cref="ArgumentNullException">If either the IRoverEngine or INavigator instances 
        /// are null</exception>
        public Rover(Direction direction, int x, int y, IRoverEngine roverEngine, INavigator navigator)
        {
            if (roverEngine == null)
                throw new ArgumentNullException("roverEngine");
            if (navigator == null)
                throw new ArgumentNullException("navigator");

            RoverEngine = roverEngine;
            Navigator = navigator;
            Direction = direction;
            Position = new Coordinate(x, y);
        }

        /// <summary>
        /// Gets or sets the direction of the rover
        /// </summary>
        public Direction Direction
        {
            get
            {
                return Navigator.Direction;
            }
            set
            {
                Navigator.Direction = value;
            }
        }

        /// <summary>
        /// Gets or sets the x-y position of the rover
        /// </summary>
        public Coordinate Position
        {
            get
            {
                return Navigator.Position;
            }
            private set
            {
                Navigator.Position = value;
            }
        }

        /// <summary>
        /// Gets or sets the engine capable of moving the rover
        /// </summary>
        public IRoverEngine RoverEngine { get; private set; }

        /// <summary>
        /// Gets or sets the navigator capable of helping the rover traverse a grid
        /// </summary>
        public INavigator Navigator { get; private set; }

        /// <summary>
        /// Moves the rover based on the given command sequence
        /// </summary>
        /// <remarks>
        /// 1. Commands are not case-sensitive
        /// 2. If the rover encounters an obstacle, the rover will stop and no further 
        ///    commands will be executed
        /// </remarks>
        /// <param name="commands">String of commands for the rover to execute</param>
        /// <exception cref="ArgumentNullException">Command sequence string is null</exception>
        /// <exception cref="InvalidOperationException">Rover is given an unrecognized command</exception>
        public void Move(string commands)
        {
            if (commands == null)
                throw new ArgumentNullException("commands");

            foreach (var movement in commands.ToUpper())
            {
                if (movement == MovementCommands.Forward)
                {
                    if (Navigator.CanMoveForward())
                        RoverEngine.MoveForward(Direction, Position);
                    else
                        break;
                }
                else if (movement == MovementCommands.Backward)
                {
                    if (Navigator.CanMoveBackward())
                        RoverEngine.MoveBackward(Direction, Position);
                    else
                        break;
                }
                else if (movement == MovementCommands.TurnLeft)
                    Direction = RoverEngine.TurnLeft(Direction);
                else if (movement == MovementCommands.TurnRight)
                    Direction = RoverEngine.TurnRight(Direction);
                else
                    throw new InvalidOperationException(InvalidCommandPrefix + movement);
            }
        }
    }
}
