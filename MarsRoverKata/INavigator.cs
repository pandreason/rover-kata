﻿namespace MarsRoverKata
{
    /// <summary>
    /// Defines the behavior of a navigator
    /// </summary>
    /// <remarks>Authors: Geoff Mazeroff, Cameron Presley, Phillip Andreason</remarks>
    public interface INavigator
    {
        /// <summary>
        /// Gets or sets the direction in which the object being navigated is facing
        /// </summary>
        Direction Direction { get; set; }

        /// <summary>
        /// Gets or sets the position of the object being navigated
        /// </summary>
        Coordinate Position { get; set; }
        
        /// <summary>
        /// Determines if forward navigation is possible
        /// </summary>
        /// <returns>True if forward movement is not impeded by an obstacle; false otherwise</returns>
        bool CanMoveForward();

        /// <summary>
        /// Determines if backward navigation is possible
        /// </summary>
        /// <returns>True if backward movement is not impeded by an obstacle; false otherwise</returns>
        bool CanMoveBackward();
    }
}
