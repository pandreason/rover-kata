﻿namespace MarsRoverKata
{
    /// <summary>
    /// Defines the behavior of a rover engine
    /// </summary>
    /// <remarks>Authors: Geoff Mazeroff, Cameron Presley, Phillip Andreason</remarks>
    public interface IRoverEngine
    {
        /// <summary>
        /// Moves the rover foward given a direction and current position
        /// </summary>
        /// <param name="direction">Direction the rover is facing</param>
        /// <param name="currentPosition">Rover's current position</param>
        void MoveForward(Direction direction, Coordinate currentPosition);

        /// <summary>
        /// Moves the rover backward given a direction and current position
        /// </summary>
        /// <param name="direction">Direction the rover is facing</param>
        /// <param name="currentPosition">Rover's current position</param>
        void MoveBackward(Direction direction, Coordinate currentPosition);
        
        /// <summary>
        /// Turns the rover left (i.e., 90 degrees counterclockwise)
        /// </summary>
        /// <param name="direction">Direction the rover is facing</param>
        /// <returns>Rover's new direction</returns>
        Direction TurnLeft(Direction direction);

        /// <summary>
        /// Turns the rover left (i.e., 90 degrees counterclockwise)
        /// </summary>
        /// <param name="direction">Direction the rover is facing</param>
        /// <returns>Rover's new direction</returns>
        Direction TurnRight(Direction direction);
    }
}
