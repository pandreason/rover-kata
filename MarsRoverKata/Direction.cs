﻿namespace MarsRoverKata
{
    /// <summary>
    /// Specifies constants defining a cardinal directions
    /// </summary>
    /// <remarks>Authors: Geoff Mazeroff, Cameron Presley, Phillip Andreason</remarks>
    public enum Direction
    {
        North,
        East,
        South,
        West
    }
}
