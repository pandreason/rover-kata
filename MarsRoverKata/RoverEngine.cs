﻿using System;

namespace MarsRoverKata
{
    /// <summary>
    /// Represents a rover engine capable of movement and turning
    /// </summary>
    /// <remarks>Authors: Geoff Mazeroff, Cameron Presley, Phillip Andreason</remarks>
    public class RoverEngine : IRoverEngine
    {
        /// <summary>
        /// Moves the rover foward given a direction and current position
        /// </summary>
        /// <param name="direction">Direction the rover is facing</param>
        /// <param name="currentPosition">Rover's current position</param>
        /// <exception cref="ArgumentNullException">Given coordinate is null</exception>
        public void MoveForward(Direction direction, Coordinate currentPosition)
        {
            if (currentPosition == null)
                throw new ArgumentNullException("currentPosition");

            switch (direction)
            {
                case Direction.North:
                    currentPosition.Y++;
                    break;
                case Direction.South:
                    currentPosition.Y--;
                    break;
                case Direction.West:
                    currentPosition.X--;
                    break;
                default:
                    currentPosition.X++;
                    break;
            }
        }

        /// <summary>
        /// Moves the rover backward given a direction and current position
        /// </summary>
        /// <param name="direction">Direction the rover is facing</param>
        /// <param name="currentPosition">Rover's current position</param>
        /// <exception cref="ArgumentNullException">Given coordinate is null</exception>
        public void MoveBackward(Direction direction, Coordinate currentPosition)
        {
            direction = TurnLeft(TurnLeft(direction));
            MoveForward(direction, currentPosition);
        }

        /// <summary>
        /// Turns the rover left (i.e., 90 degrees counterclockwise)
        /// </summary>
        /// <param name="direction">Direction the rover is facing</param>
        /// <returns>Rover's new direction</returns>
        public Direction TurnLeft(Direction direction)
        {
            var leftDirectionIndex = ((int)direction + 3) % 4;
            return (Direction)leftDirectionIndex;
        }

        /// <summary>
        /// Turns the rover left (i.e., 90 degrees counterclockwise)
        /// </summary>
        /// <param name="direction">Direction the rover is facing</param>
        /// <returns>Rover's new direction</returns>
        public Direction TurnRight(Direction direction)
        {
            return TurnLeft(TurnLeft(TurnLeft(direction)));
        }
    }
}
