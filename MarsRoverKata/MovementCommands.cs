﻿namespace MarsRoverKata
{
    /// <summary>
    /// Defines constants for rover movement commands
    /// </summary>
    /// <remarks>Authors: Geoff Mazeroff, Cameron Presley, Phillip Andreason</remarks>
    public static class MovementCommands
    {
        /// <summary>
        /// Command to move the rover forward one position
        /// </summary>
        public const char Forward = 'F';

        /// <summary>
        /// Command to move the rover backward one position
        /// </summary>
        public const char Backward = 'B';

        /// <summary>
        /// Command to move the rover left one position
        /// </summary>
        public const char TurnLeft = 'L';

        /// <summary>
        /// Command to move the rover right one position
        /// </summary>
        public const char TurnRight = 'R';
    }
}
