﻿using System;
using System.Collections.Generic;

namespace MarsRoverKata
{
    /// <summary>
    /// Represents a navigator that provides support for an infinite (i.e., wrapping) grid
    /// and recognition of obstacles
    /// </summary>
    /// <remarks>Authors: Geoff Mazeroff, Cameron Presley, Phillip Andreason</remarks>
    public class Navigator : INavigator
    {
        private readonly int _maxXBoundary;
        private readonly int _maxYBoundary;
        private readonly List<Coordinate> _obstacles;
        private Coordinate _position;

        /// <summary>
        /// Creates a new Navigator instance based on the given grid specifications and 
        /// list of obstacle coordinates
        /// </summary>
        /// <param name="maxXBoundary">Maximum number of columns</param>
        /// <param name="maxYBoundary">Maximum number of rows</param>
        /// <param name="obstacles">List of coordinates where obstacles are located</param>
        /// <exception cref="ArgumentOutOfRangeException">Either boundary is less than one</exception>
        /// <exception cref="ArgumentNullException">List of obstacles is null</exception>
        public Navigator(int maxXBoundary, int maxYBoundary, List<Coordinate> obstacles)
        {
            if (maxXBoundary < 1)
                throw new ArgumentOutOfRangeException("maxYBoundary: " + maxXBoundary);
            if (maxYBoundary < 1)
                throw new ArgumentOutOfRangeException("maxYBoundary: " + maxYBoundary);
            if (obstacles == null)
                throw new ArgumentNullException("obstacles");

            _maxXBoundary = maxXBoundary;
            _maxYBoundary = maxYBoundary;
            _position = new Coordinate(0, 0);
            _obstacles = obstacles;
        }

        /// <summary>
        /// Gets or sets the direction in which the object being navigated is facing
        /// </summary>
        public Direction Direction { get; set; }

        /// <summary>
        /// Gets or sets the position of the object being navigated. If the Coordinate is
        /// outside the bounds of the grid, the coordinate will be normalized (i.e., wrapped).
        /// </summary>
        /// <exception cref="NotSupportedException">The given position is occupied by an obstacle</exception>
        public Coordinate Position
        {
            get
            {
                NormalizePosition(_position);
                return _position;
            }

            set
            {
                if (_obstacles.Contains(value))
                    throw new NotSupportedException();

                _position = value;
            }
        }

        /// <summary>
        /// Determines if forward navigation is possible
        /// </summary>
        /// <returns>True if forward movement is not impeded by an obstacle; false otherwise</returns>
        public bool CanMoveForward()
        {
            var temp = new Coordinate(Position.X, Position.Y);
            switch (Direction)
            {
                case Direction.East:
                    temp.X++;
                    break;
                case Direction.West:
                    temp.X--;
                    break;
                case Direction.North:
                    temp.Y++;
                    break;
                default:
                    temp.Y--;
                    break;
            }

            NormalizePosition(temp);
            return !_obstacles.Contains(temp);
        }

        /// <summary>
        /// Determines if backward navigation is possible
        /// </summary>
        /// <returns>True if backward movement is not impeded by an obstacle; false otherwise</returns>
        public bool CanMoveBackward()
        {
            TurnAround();
            var canMoveBackward = CanMoveForward();
            TurnAround();
            return canMoveBackward;
        }

        /// <summary>
        /// Normalizes the X and Y values of the position to support edge wrapping
        /// </summary>
        /// <param name="position">Position to be normalized</param>
        private void NormalizePosition(Coordinate position)
        {
            position.X = (position.X + _maxXBoundary) % _maxXBoundary;
            position.Y = (position.Y + _maxYBoundary) % _maxYBoundary;
        }

        /// <summary>
        /// Turns the object to be navigated 180 degrees
        /// </summary>
        private void TurnAround()
        {
            var turnAroundDirectionIndex = ((int)Direction + 2) % 4;
            Direction = (Direction)turnAroundDirectionIndex;
        }
    }
}
