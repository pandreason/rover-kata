﻿namespace MarsRoverKata
{
    /// <summary>
    /// Represents a simple Cartesian coordinate
    /// </summary>
    /// <remarks>Authors: Geoff Mazeroff, Cameron Presley, Phillip Andreason</remarks>
    public class Coordinate
    {
        /// <summary>
        /// Creates a new Coordinate instance with the given x- and y-values
        /// </summary>
        /// <param name="x">x value</param>
        /// <param name="y">y value</param>
        public Coordinate(int x, int y)
        {
            X = x;
            Y = y;
        }
        
        /// <summary>
        /// Gets or sets the X value of the coordinate
        /// </summary>
        public int X { get; set; }

        /// <summary>
        /// Gets or sets the Y value of thhe coordintae
        /// </summary>
        public int Y { get; set; }

        /// <summary>
        /// Determines whether or not this Coordinate is the same as the given object
        /// </summary>
        /// <param name="obj">Object to compare with</param>
        /// <returns>True if this Coordinate is the same as the given object; false otherwise</returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((Coordinate) obj);
        }

        /// <summary>
        /// Determines whether or not this Coordinate is the same as the given Coordinate
        /// </summary>
        /// <param name="other">Coordinate to compare with</param>
        /// <returns>True if this Coordinate is the same as the given Coordinate; false otherwise</returns>
        protected bool Equals(Coordinate other)
        {
            return X == other.X && Y == other.Y;
        }

        /// <summary>
        /// Gets the hashcode of this Coordinate
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            unchecked
            {
                return (X * 397) ^ Y;
            }
        }

        /// <summary>
        /// Returns a string representation of the coordinate
        /// </summary>
        /// <returns>String representation of the coordinate</returns>
        public override string ToString()
        {
            return string.Format("X: {0}, Y: {1}", X, Y);
        }
    }

}
