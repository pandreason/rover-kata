﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace MarsRoverKata.Tests.Integration_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class Given_that_a_rover_encounters_an_obstacle
    {
        private Coordinate _expectedPosition;
        private Rover _rover;

        [TestInitialize]
        public void TestInitialize()
        {
            _expectedPosition = new Coordinate(1, 0);
            _rover = new Rover(Direction.East, 0, 0, new RoverEngine(), 
                new Navigator(3, 2, new List<Coordinate> { new Coordinate(1, 1) }));
        }

        [TestMethod]
        public void Then_the_rover_will_go_as_far_forward_as_it_can_and_then_stop()
        {
            _rover.Move("frflf");

            Assert.AreEqual(_expectedPosition, _rover.Position);
        }

        [TestMethod]
        public void Then_the_rover_will_go_as_far_backward_as_it_can_and_then_stop()
        {
            _rover.Move("bbrblb");

            Assert.AreEqual(_expectedPosition, _rover.Position);
        }
    }
    // ReSharper restore InconsistentNaming
}
