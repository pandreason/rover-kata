﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace MarsRoverKata.Tests.Integration_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class Given_that_a_rover_has_a_navigator
    {
        [TestMethod]
        public void Then_the_rover_uses_the_navigator_to_move_forward_north_with_wrapping()
        {
            var rover = new Rover(Direction.North, 0, 0, new RoverEngine(), new Navigator(2, 2, new List<Coordinate>()));
            
            rover.Move("FFF");

            Assert.AreEqual(new Coordinate(0, 1), rover.Position);
        }

        [TestMethod]
        public void Then_the_rover_uses_the_navigator_to_move_backward_north_with_wrapping()
        {
            var rover = new Rover(Direction.North, 0, 0, new RoverEngine(), new Navigator(2, 2, new List<Coordinate>()));
            
            rover.Move("BBB");

            Assert.AreEqual(new Coordinate(0, 1), rover.Position);
        }

        [TestMethod]
        public void Then_the_rover_uses_the_navigator_to_move_forward_west_with_wrapping()
        {
            var rover = new Rover(Direction.West, 0, 0, new RoverEngine(), new Navigator(2, 2, new List<Coordinate>()));
            
            rover.Move("FFF");

            Assert.AreEqual(new Coordinate(1, 0), rover.Position);
        }

        [TestMethod]
        public void Then_the_rover_uses_the_navigator_to_move_backward_west_with_wrapping()
        {
            var rover = new Rover(Direction.West, 0, 0, new RoverEngine(), new Navigator(2, 2, new List<Coordinate>()));

            rover.Move("BBB");

            Assert.AreEqual(new Coordinate(1, 0), rover.Position);
        }
    }
    // ReSharper restore InconsistentNaming
}
