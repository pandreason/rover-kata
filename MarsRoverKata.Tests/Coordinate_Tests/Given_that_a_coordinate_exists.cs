﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MarsRoverKata.Tests.Coordinate_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class Given_that_a_coordinate_exists
    {
        private static Coordinate _coord;

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            _coord = new Coordinate(4, -87);
        }

        [TestMethod]
        public void Then_the_string_representation_is_correct()
        {
            const string expectedString = "X: 4, Y: -87";

            Assert.AreEqual(expectedString, _coord.ToString());
        }

        [TestMethod]
        public void Then_a_reference_is_not_equal_to_a_null_instance()
        {
            Assert.IsFalse(_coord.Equals(null));
        }

        [TestMethod]
        public void Then_a_reference_is_not_equal_to_something_other_than_a_Coordinate()
        {
            // ReSharper disable SuspiciousTypeConversion.Global
            Assert.IsFalse(_coord.Equals(1));
            // ReSharper restore SuspiciousTypeConversion.Global
        }

        [TestMethod]
        public void Then_a_coordinate_is_equal_to_itself()
        {
            // ReSharper disable EqualExpressionComparison
            Assert.IsTrue(_coord.Equals(_coord));
            // ReSharper restore EqualExpressionComparison
        }

        [TestMethod]
        public void Then_two_coordinates_with_the_same_x_and_y_values_are_equal()
        {
            var otherCoord = new Coordinate(_coord.X, _coord.Y);
            Assert.IsTrue(_coord.Equals(otherCoord));
        }

        [TestMethod]
        public void Then_two_coordinates_with_different_x_and_y_values_are_not_equal()
        {
            var otherCoord = new Coordinate(_coord.X+1, _coord.Y-1);
            Assert.IsFalse(_coord.Equals(otherCoord));
        }

        [TestMethod]
        public void Then_its_hashcode_is_correct()
        {
            var expectedHashCode = (_coord.X * 397) ^ _coord.Y;
            Assert.AreEqual(expectedHashCode, _coord.GetHashCode());
        }

        // ReSharper restore InconsistentNaming
    }
}
