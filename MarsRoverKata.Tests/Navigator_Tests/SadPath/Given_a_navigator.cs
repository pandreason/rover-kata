﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace MarsRoverKata.Tests.Navigator_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    // ReSharper disable UnusedVariable
    [TestClass]
    public class Given_a_navigator
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void And_the_list_of_obstacles_is_null_then_an_exception_is_thrown()
        {

            var navigator = new Navigator(1, 1, null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void And_the_maximum_x_boundary_is_less_than_one_then_an_exception_is_thrown()
        {
            var navigator = new Navigator (0,1,new List<Coordinate>());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void And_the_maximum_y_boundary_is_less_than_one_then_an_exception_is_thrown()
        {
            var navigator = new Navigator(1, 0, new List<Coordinate>());
        }

        [TestMethod]
        [ExpectedException(typeof(NotSupportedException))]
        public void And_the_posistion_is_on_top_of_an_obstacle_then_an_exception_is_thrown()
        {
            var navigator = new Navigator(10, 10, new List<Coordinate> { new Coordinate(5, 5) });
            navigator.Position = new Coordinate(5, 5);
        }
    }
    // ReSharper restore InconsistentNaming
    // ReSharper restore UnusedVariable
}
