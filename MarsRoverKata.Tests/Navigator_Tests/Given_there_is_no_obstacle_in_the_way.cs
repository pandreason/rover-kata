﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MarsRoverKata.Tests.Navigator_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class Given_there_is_no_obstacle_in_the_way
    {
        private const int GridSize = 10;

        [TestMethod]
        public void And_trying_to_navigate_forward_facing_north_then_navigation_is_allowed()
        {
            var navigator = new Navigator(GridSize, GridSize, new List<Coordinate>())
                {
                    Position = new Coordinate(0, 0),
                    Direction = Direction.North
                };

            Assert.IsTrue(navigator.CanMoveForward());
        }

        [TestMethod]
        public void And_trying_to_navigate_forward_facing_east_then_navigation_is_allowed()
        {
            var navigator = new Navigator(GridSize, GridSize, new List<Coordinate> { new Coordinate(0, 1) })
                {
                    Position = new Coordinate(0, 0),
                    Direction = Direction.East
                };

            Assert.IsTrue(navigator.CanMoveForward());
        }

        [TestMethod]
        public void And_trying_to_navigate_forward_facing_west_then_navigation_is_allowed()
        {
            var navigator = new Navigator(GridSize, GridSize, new List<Coordinate> { new Coordinate(0, GridSize - 1) })
                {
                    Position = new Coordinate(0, 0),
                    Direction = Direction.West
                };

            Assert.IsTrue(navigator.CanMoveForward());
        }

        [TestMethod]
        public void And_trying_to_navigate_forward_facing_south_then_navigation_is_allowed()
        {
            var navigator = new Navigator(GridSize, GridSize, new List<Coordinate> { new Coordinate(1, GridSize - 1) })
                {
                    Position = new Coordinate(0, 0),
                    Direction = Direction.South
                };

            Assert.IsTrue(navigator.CanMoveForward());
        }

        [TestMethod]
        public void And_trying_to_navigate_backward_facing_north_then_navigation_is_allowed()
        {
            var navigator = new Navigator(GridSize, GridSize, new List<Coordinate> { new Coordinate(1, 0) })
                {
                    Position = new Coordinate(0, 1),
                    Direction = Direction.North
                };

            Assert.IsTrue(navigator.CanMoveBackward());
        }

        [TestMethod]
        public void And_trying_to_navigate_backward_facing_south_then_navigation_is_allowed()
        {
            var navigator = new Navigator(GridSize, GridSize, new List<Coordinate> { new Coordinate(1, 0) })
                {
                    Position = new Coordinate(0, 1),
                    Direction = Direction.South
                };

            Assert.IsTrue(navigator.CanMoveBackward());
        }

        [TestMethod]
        public void And_trying_to_navigate_backward_facing_east_then_navigation_is_allowed()
        {
            var navigator = new Navigator(GridSize, GridSize, new List<Coordinate> { new Coordinate(1, 1) })
                {
                    Position = new Coordinate(1, 0),
                    Direction = Direction.North
                };

            Assert.IsTrue(navigator.CanMoveBackward());
        }

        [TestMethod]
        public void And_trying_to_navigate_backward_facing_west_then_navigation_is_allowed()
        {
            var navigator = new Navigator(GridSize, GridSize, new List<Coordinate> { new Coordinate(0, 0) })
                {
                    Position = new Coordinate(0, 1),
                    Direction = Direction.West
                };

            Assert.IsTrue(navigator.CanMoveBackward());
        }
    }
    // ReSharper restore InconsistentNaming
}
