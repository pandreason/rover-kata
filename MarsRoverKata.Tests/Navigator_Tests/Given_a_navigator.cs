﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MarsRoverKata.Tests.Navigator_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class Given_a_navigator
    {
        private const int GridSize = 5;

        private static Navigator _navigator;
        
        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            _navigator = new Navigator(GridSize, GridSize, new List<Coordinate>());
        }
        
        [TestMethod]
        public void And_at_the_rightmost_boundary_of_the_grid_and_setting_the_x_position_after_it_then_the_x_value_is_the_leftmost_boundary()
        {            
            _navigator.Position.X = GridSize;

            Assert.AreEqual(0, _navigator.Position.X);
        }

        [TestMethod]
        public void And_at_the_rightmost_boundary_of_the_grid_and_setting_the_x_position_two_after_it_then_the_x_value_is_the_leftmost_boundary_plus_one()
        {   
            _navigator.Position.X = GridSize + 1;

            Assert.AreEqual(1, _navigator.Position.X);
        }

        [TestMethod]
        public void And_at_the_leftmost_boundary_of_the_grid_and_setting_the_x_position_one_before_it_then_the_x_value_is_the_rightmost_boundary()
        {
            _navigator.Position.X = -1;

            Assert.AreEqual(GridSize - 1, _navigator.Position.X);
        }

        [TestMethod]
        public void And_at_the_topmost_boundary_of_the_grid_and_setting_the_y_position_above_it_then_the_y_value_is_the_bottommost_boundary()
        {
            _navigator.Position.Y = GridSize;

            Assert.AreEqual(0, _navigator.Position.Y);
        }

        [TestMethod]
        public void And_at_the_topmost_boundary_of_the_grid_and_setting_the_y_position_two_above_it_then_the_y_value_is_the_bottom_boundary_plus_1()
        {
            _navigator.Position.Y = GridSize + 1;

            Assert.AreEqual(1, _navigator.Position.Y);
        }

        [TestMethod]
        public void And_at_the_bottommost_boundary_of_the_grid_and_setting_the_y_position_one_below_it_then_the_y_value_is_the_top_boundary()
        {
            _navigator.Position.Y = -1;

            Assert.AreEqual(GridSize-1, _navigator.Position.Y);
        }

        [TestMethod]
        public void And_the_initial_position_is_set_outside_of_the_grid_range_then_the_position_is_normalized()
        {
            _navigator.Position = new Coordinate(GridSize + 2, GridSize + 2);

            var expected = new Coordinate(2, 2);

            Assert.AreEqual(expected, _navigator.Position);
        }
        // ReSharper restore InconsistentNaming
    }
}
