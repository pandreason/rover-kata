﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace MarsRoverKata.Tests.Navigator_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class Given_there_is_an_obstacle_in_the_way
    {
        private const int GridSize = 10;

        [TestMethod]
        public void And_trying_to_navigate_forward_facing_north_then_navigation_is_not_allowed()
        {
            var navigator = new Navigator(GridSize, GridSize, 
                new List<Coordinate> { new Coordinate(0, 1) })
                {
                    Position = new Coordinate(0, 0),
                    Direction = Direction.North
                };

            Assert.IsFalse(navigator.CanMoveForward());
        }

        [TestMethod]
        public void And_trying_to_navigate_forward_facing_south_then_navigation_is_not_allowed()
        {
            var navigator = new Navigator(GridSize, GridSize,
                new List<Coordinate> { new Coordinate(0, 0) })
                {
                    Position = new Coordinate(0, 1),
                    Direction = Direction.South
                };

            Assert.IsFalse(navigator.CanMoveForward());
        }

        [TestMethod]
        public void And_trying_to_navigate_forward_facing_west_then_navigation_is_not_allowed()
        {
            var navigator = new Navigator(GridSize, GridSize,
                new List<Coordinate> { new Coordinate(0, 0) })
                {
                    Position = new Coordinate(1, 0),
                    Direction = Direction.West
                };

            Assert.IsFalse(navigator.CanMoveForward());
        }

        [TestMethod]
        public void And_trying_to_navigate_forward_facing_east_then_navigation_is_not_allowed()
        {
            var navigator = new Navigator(GridSize, GridSize,
                new List<Coordinate> { new Coordinate(1, 0) })
                {
                    Position = new Coordinate(0, 0),
                    Direction = Direction.East
                };

            Assert.IsFalse(navigator.CanMoveForward());
        }

        [TestMethod]
        public void And_trying_to_navigate_forward_facing_south_with_wrapping_then_navigation_is_not_allowed()
        {
            var navigator = new Navigator(GridSize, GridSize,
                new List<Coordinate> { new Coordinate(0, GridSize - 1) })
                {
                    Position = new Coordinate(0, 0),
                    Direction = Direction.South
                };

            Assert.IsFalse(navigator.CanMoveForward());
        }
        
        [TestMethod]
        public void And_trying_to_navigate_forward_facing_north_with_wrapping_then_navigation_is_not_allowed()
        {
            var navigator = new Navigator(GridSize, GridSize, new List<Coordinate> { new Coordinate(0, 0) })
                {
                    Position = new Coordinate(0, GridSize - 1),
                    Direction = Direction.North
                };

            Assert.IsFalse(navigator.CanMoveForward());
        }

        [TestMethod]
        public void And_trying_to_navigate_forward_facing_east_with_wrapping_then_navigation_is_not_allowed()
        {
            var navigator = new Navigator(GridSize, GridSize, new List<Coordinate> { new Coordinate(0, 0) })
                {
                    Position = new Coordinate(GridSize - 1, 0),
                    Direction = Direction.East
                };

            Assert.IsFalse(navigator.CanMoveForward());
        }

        [TestMethod]
        public void And_trying_to_navigate_forward_facing_west_with_wrapping_then_navigation_is_not_allowed()
        {
            var navigator = new Navigator(GridSize, GridSize, new List<Coordinate> { new Coordinate(0, 0) })
                {
                    Position = new Coordinate(1, 0),
                    Direction = Direction.West
                };

            Assert.IsFalse(navigator.CanMoveForward());
        }

        [TestMethod]
        public void And_trying_to_navigate_backward_facing_north_then_navigation_is_not_allowed()
        {
            var navigator = new Navigator(GridSize, GridSize, new List<Coordinate> { new Coordinate(0, 0) })
                {
                    Position = new Coordinate(0, 1),
                    Direction = Direction.North
                };

            Assert.IsFalse(navigator.CanMoveBackward());
        }

        [TestMethod]
        public void And_trying_to_navigate_backward_facing_south_then_navigation_is_not_allowed()
        {
            var navigator = new Navigator(GridSize, GridSize, new List<Coordinate> { new Coordinate(0, 1) })
                {
                    Position = new Coordinate(0, 0),
                    Direction = Direction.South
                };

            Assert.IsFalse(navigator.CanMoveBackward());
        }

        [TestMethod]
        public void And_trying_to_navigate_backward_facing_east_then_navigation_is_not_allowed()
        {
            var navigator = new Navigator(GridSize, GridSize, new List<Coordinate> { new Coordinate(0, 0) })
                {
                    Position = new Coordinate(1, 0),
                    Direction = Direction.East
                };

            Assert.IsFalse(navigator.CanMoveBackward());
        }

        [TestMethod]
        public void And_trying_to_navigate_backward_facing_west_then_navigation_is_not_allowed()
        {
            var navigator = new Navigator(GridSize, GridSize, new List<Coordinate> { new Coordinate(1, 0) })
                {
                    Position = new Coordinate(0, 0),
                    Direction = Direction.West
                };

            Assert.IsFalse(navigator.CanMoveBackward());
        }

        [TestMethod]
        public void And_trying_to_navigate_backward_facing_north_with_wrapping_then_navigation_is_not_allowed()
        {
            var navigator = new Navigator(GridSize, GridSize, new List<Coordinate> { new Coordinate(0, GridSize - 1) })
                {
                    Position = new Coordinate(0, 0),
                    Direction = Direction.North
                };

            Assert.IsFalse(navigator.CanMoveBackward());
        }

        [TestMethod]
        public void And_trying_to_navigate_backward_facing_south_with_wrapping_then_navigation_is_not_allowed()
        {
            var navigator = new Navigator(GridSize, GridSize, new List<Coordinate> { new Coordinate(0, 0) })
                {
                    Position = new Coordinate(0, GridSize - 1),
                    Direction = Direction.South
                };

            Assert.IsFalse(navigator.CanMoveBackward());
        }

        [TestMethod]
        public void And_trying_to_navigate_backward_facing_east_with_wrapping_then_navigation_is_not_allowed()
        {
            var navigator = new Navigator(GridSize, GridSize, new List<Coordinate> { new Coordinate(GridSize - 1, 0) })
                {
                    Position = new Coordinate(0, 0),
                    Direction = Direction.East
                };

            Assert.IsFalse(navigator.CanMoveBackward());
        }

        [TestMethod]
        public void And_trying_to_navigate_backward_facing_west_with_wrapping_then_navigation_is_not_allowed()
        {
            var navigator = new Navigator(GridSize, GridSize, new List<Coordinate> { new Coordinate(0, 0) })
                {
                    Position = new Coordinate(GridSize - 1, 0),
                    Direction = Direction.West
                };

            Assert.IsFalse(navigator.CanMoveBackward());
        }
    }
    // ReSharper restore InconsistentNaming
}
