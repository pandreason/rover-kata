﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MarsRoverKata.Tests.RoverEngine_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class Given_that_a_rover_engine_should_turn_right
    {
        private static RoverEngine _engine;

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            _engine = new RoverEngine();
        }

        [TestMethod]
        public void And_the_rover_is_facing_north_then_the_rover_should_turn_right_to_face_east()
        {
            var roverDirection = Direction.North;

            roverDirection = _engine.TurnRight(roverDirection);

            Assert.AreEqual(Direction.East, roverDirection);
        }

        [TestMethod]
        public void And_the_rover_is_facing_east_then_the_rover_should_turn_right_to_face_south()
        {
            var roverDirection = Direction.East;

            roverDirection = _engine.TurnRight(roverDirection);

            Assert.AreEqual(Direction.South, roverDirection);
        }

        [TestMethod]
        public void And_the_rover_is_facing_south_then_the_rover_should_turn_right_to_face_west()
        {
            var roverDirection = Direction.South;

            roverDirection = _engine.TurnRight(roverDirection);

            Assert.AreEqual(Direction.West, roverDirection);
        }

        [TestMethod]
        public void And_the_rover_is_facing_West_then_the_rover_should_turn_right_to_face_North()
        {
            var roverDirection = Direction.West;

            roverDirection = _engine.TurnRight(roverDirection);

            Assert.AreEqual(Direction.North, roverDirection);
        }
    }
    // ReSharper restore InconsistentNaming
}
