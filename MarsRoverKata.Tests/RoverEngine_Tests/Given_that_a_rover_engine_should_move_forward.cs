﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MarsRoverKata.Tests.RoverEngine_Tests
{
    // ReSharper disable InconsistentNaming
    // ReSharper disable RedundantArgumentName
    [TestClass]
    public class Given_that_a_rover_engine_should_move_forward
    {
        private static RoverEngine _engine;
        private static Coordinate _roverPosition;

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            _engine = new RoverEngine();
        }

        [TestInitialize]
        public void TestInitialize()
        {
            _roverPosition = new Coordinate(x: 0, y: 0);
        }

        [TestMethod]
        public void And_the_rover_is_facing_north_then_the_rover_moves_forward_north()
        {
            var expectedPosition = new Coordinate(x: 0, y: 1);

            _engine.MoveForward(Direction.North, _roverPosition);

            Assert.AreEqual(expectedPosition, _roverPosition);
        }

        [TestMethod]
        public void And_the_rover_is_facing_south_then_the_rover_moves_forward_south()
        {
            var expectedPosition = new Coordinate(x: 0, y: -1);

            _engine.MoveForward(Direction.South, _roverPosition);

            Assert.AreEqual(expectedPosition, _roverPosition);
        }

        [TestMethod]
        public void And_the_rover_is_facing_west_then_the_rover_moves_forward_west()
        {
            var expectedPosition = new Coordinate(x: -1, y: 0);

            _engine.MoveForward(Direction.West, _roverPosition);

            Assert.AreEqual(expectedPosition, _roverPosition);
        }

        [TestMethod]
        public void And_the_rover_is_facing_east_then_the_rover_moves_forward_east()
        {
            var expectedPosition = new Coordinate(x: 1, y: 0);

            _engine.MoveForward(Direction.East, _roverPosition);

            Assert.AreEqual(expectedPosition, _roverPosition);
        }

        [TestMethod]
        public void And_the_rover_is_facing_north_then_the_rover_moves_forward_north_twice()
        {
            var expectedPosition = new Coordinate(x: 0, y: 2);

            _engine.MoveForward(Direction.North, _roverPosition);
             _engine.MoveForward(Direction.North, _roverPosition);

            Assert.AreEqual(expectedPosition, _roverPosition);
        }

        [TestMethod]
        public void And_the_rover_is_facing_south_then_the_rover_moves_forward_south_twice()
        {
            var expectedPosition = new Coordinate(x: 0, y: -2);

            _engine.MoveForward(Direction.South,  _roverPosition);
            _engine.MoveForward(Direction.South,  _roverPosition);

            Assert.AreEqual(expectedPosition, _roverPosition);
        }

        [TestMethod]
        public void And_the_rover_is_facing_east_then_the_rover_moves_forward_east_twice()
        {
            var expectedPosition = new Coordinate(x: 2, y: 0);

            _engine.MoveForward(Direction.East, _roverPosition);
            _engine.MoveForward(Direction.East, _roverPosition);

            Assert.AreEqual(expectedPosition, _roverPosition);
        }

        [TestMethod]
        public void And_the_rover_is_facing_west_then_the_rover_moves_forward_west_twice()
        {
            var expectedPosition = new Coordinate(x: -2, y: 0);

            _engine.MoveForward(Direction.West, _roverPosition);
            _engine.MoveForward(Direction.West,  _roverPosition);

            Assert.AreEqual(expectedPosition, _roverPosition);
        }
    }
    // ReSharper restore InconsistentNaming
    // ReSharper restore RedundantArgumentName
}
