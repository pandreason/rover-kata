﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MarsRoverKata.Tests.RoverEngine_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class Given_that_a_rover_engine_can_move
    {
        private static IRoverEngine _engine;

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            _engine = new RoverEngine();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void And_the_position_to_move_forward_from_is_null_then_an_exception_is_thrown()
        {
            _engine.MoveForward(Direction.North, null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void And_the_position_to_move_backward_from_is_null_then_an_exception_is_thrown()
        {
            _engine.MoveBackward(Direction.North, null);
        }
    }
    // ReSharper restore InconsistentNaming
}
