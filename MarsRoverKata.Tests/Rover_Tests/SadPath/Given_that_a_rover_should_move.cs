﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace MarsRoverKata.Tests.Rover_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class Given_that_a_rover_should_move
    {
        private static IRoverEngine _dummyRoverEngine;
        private static INavigator _dummyNavigator;
        private static Rover _rover;

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            _dummyRoverEngine = MockRepository.GenerateStub<IRoverEngine>();
            _dummyNavigator = MockRepository.GenerateStub<INavigator>();
            _dummyNavigator.Stub(n => n.CanMoveForward()).Return(true);
            _dummyNavigator.Stub(n => n.CanMoveBackward()).Return(true);
            _rover = new Rover(Direction.North, 0, 0, _dummyRoverEngine, _dummyNavigator);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void And_the_string_of_movement_commands_is_null_then_an_exception_is_thrown()
        {
            _rover.Move(null);
        }

        [TestMethod]
        public void And_the_string_of_movement_commands_has_invalid_characters_then_an_exception_is_thrown()
        {

            const string expectedExceptionMessage = Rover.InvalidCommandPrefix + "Q";
            var actualExceptionMessage = String.Empty;

            try
            {
               _rover.Move("ffbqrrl");
            }
            catch (InvalidOperationException ex)
            {
                actualExceptionMessage = ex.Message;
            }

            Assert.AreEqual(expectedExceptionMessage, actualExceptionMessage);
        }
    }
    // ReSharper restore InconsistentNaming
}
