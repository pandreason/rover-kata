﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace MarsRoverKata.Tests.Rover_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    // ReSharper disable UnusedVariable
    [TestClass]
    public class Given_that_a_rover_can_be_constructed
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void And_the_IRoverEngine_instance_is_null_then_an_exception_is_thrown()
        {
            var dummyNavigator = MockRepository.GenerateStub<INavigator>();
            var rover = new Rover(Direction.North, 0, 0, null, dummyNavigator);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void And_the_INavigator_instance_is_null_then_an_exception_is_thrown()
        {
            var dummyRoverEngine = MockRepository.GenerateStub<IRoverEngine>();
            var rover = new Rover(Direction.North, 0, 0, dummyRoverEngine, null);
        }
    }
    // ReSharper restore InconsistentNaming
    // ReSharper restore UnusedVariable
}
