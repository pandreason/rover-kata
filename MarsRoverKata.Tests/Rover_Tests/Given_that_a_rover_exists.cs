﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace MarsRoverKata.Tests.Rover_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class Given_that_a_rover_exists
    {
        private static Rover _rover;
        private static IRoverEngine _dummyRoverEngine;
        private static INavigator _stubNavigator;

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            _dummyRoverEngine = MockRepository.GenerateStub<IRoverEngine>();
            _stubNavigator = MockRepository.GenerateStub<INavigator>();
            _stubNavigator.Position = new Coordinate(0,0);
            _rover = new Rover(Direction.North, 0, 0, _dummyRoverEngine, _stubNavigator);
        }

        [TestMethod]
        public void Then_it_has_a_starting_coordinate()
        {
            var expected = new Coordinate(x: 0, y: 0);
            
            Assert.AreEqual(expected, _rover.Position);
        }

        [TestMethod]
        public void And_it_is_given_a_direction_of_north_to_face_then_it_faces_that_direction()
        {
            _rover.Direction = Direction.North;
            Assert.AreEqual(Direction.North, _rover.Direction);
        }

        [TestMethod]
        public void And_it_is_given_a_direction_of_south_to_face_then_it_faces_that_direction()
        {
            _rover.Direction = Direction.South;
            Assert.AreEqual(Direction.South, _rover.Direction);
        }

        [TestMethod]
        public void And_it_is_given_a_direction_of_east_to_face_then_it_faces_that_direction()
        {
            _rover.Direction = Direction.East;
            Assert.AreEqual(Direction.East, _rover.Direction);
        }

        [TestMethod]
        public void And_it_is_given_a_direction_of_west_to_face_then_it_faces_that_direction()
        {
            _rover.Direction = Direction.West;
            Assert.AreEqual(Direction.West, _rover.Direction);
        }

        [TestMethod]
        public void Then_the_rover_engine_is_set_correctly()
        {
            Assert.AreEqual(_dummyRoverEngine, _rover.RoverEngine);            
        }

        [TestMethod]
        public void Then_the_navigator_is_set_correctly()
        {
            Assert.AreEqual(_stubNavigator, _rover.Navigator);
        }
    }
    // ReSharper restore InconsistentNaming
}
