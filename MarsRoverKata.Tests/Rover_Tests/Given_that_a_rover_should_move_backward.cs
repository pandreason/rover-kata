﻿using System.Globalization;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace MarsRoverKata.Tests.Rover_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class Given_that_a_rover_should_move_backward
    {
        private static Direction _roverDirection;
        private static Coordinate _roverPosition;
        private static IRoverEngine _mockEngine;
        private static INavigator _stubNavigator;

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            _roverDirection = Direction.South;
            _roverPosition = new Coordinate(3, 4);
            _stubNavigator = MockRepository.GenerateStub<INavigator>();
            _stubNavigator.Position = new Coordinate(0, 0);
            _stubNavigator.Stub(n => n.CanMoveBackward()).Return(true);
        }

        [TestInitialize]
        public void TestInitialize()
        {
            _mockEngine = MockRepository.GenerateStrictMock<IRoverEngine>();
        }

        [TestMethod]
        public void And_moving_backward_once_then_the_rover_engine_is_used_correctly()
        {
            _mockEngine.Expect(e => e.MoveBackward(_roverDirection, _roverPosition));

            var rover = new Rover(_roverDirection, _roverPosition.X, _roverPosition.Y, _mockEngine, _stubNavigator);

            // Act
            rover.Move(MovementCommands.Backward.ToString(CultureInfo.InvariantCulture));

            // Assert
            _mockEngine.VerifyAllExpectations();
        }

        [TestMethod]
        public void And_moving_backward_twice_then_the_rover_engine_is_used_correctly()
        {
            _mockEngine.Expect(e => e.MoveBackward(_roverDirection, _roverPosition)).Repeat.Twice();

            var rover = new Rover(_roverDirection, _roverPosition.X, _roverPosition.Y, _mockEngine,_stubNavigator);

            // Act
            rover.Move(MovementCommands.Backward.ToString(CultureInfo.InvariantCulture));
            rover.Move(MovementCommands.Backward.ToString(CultureInfo.InvariantCulture));

            // Assert
            _mockEngine.VerifyAllExpectations();
        }
    }
    // ReSharper restore InconsistentNaming
}
