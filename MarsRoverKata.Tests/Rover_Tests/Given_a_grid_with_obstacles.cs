﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace MarsRoverKata.Tests.Rover_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class Given_a_grid_with_obstacles
    {
        private static IRoverEngine _mockRoverEngine;
        private static INavigator _mockForwardNavigator, _mockBackwardNavigator;

        [TestInitialize]
        public void TestInitialize()
        {
            _mockRoverEngine = MockRepository.GenerateStrictMock<IRoverEngine>();
            
            _mockForwardNavigator = MockRepository.GenerateMock<INavigator>();
            _mockBackwardNavigator = MockRepository.GenerateMock<INavigator>();

            _mockForwardNavigator.Expect(n => n.CanMoveForward()).Return(false);
            _mockForwardNavigator.Stub(n => n.Position).Return(new Coordinate(0, 0));

            _mockBackwardNavigator.Expect(n => n.CanMoveBackward()).Return(false);
            _mockBackwardNavigator.Stub(n => n.Position).Return(new Coordinate(0, 0));
        }

        [TestMethod]
        public void And_the_rover_facing_north_attempts_to_move_forward_then_it_uses_the_navigator_to_see_if_the_rover_can_move()
        {
            var rover = new Rover(Direction.North, 0, 0, _mockRoverEngine, _mockForwardNavigator);

            rover.Move("f");

            _mockRoverEngine.VerifyAllExpectations();
            _mockForwardNavigator.VerifyAllExpectations();
        }

        [TestMethod]
        public void And_the_rover_facing_east_attempts_to_move_forward_then_it_uses_the_navigator_to_see_if_the_rover_can_move()
        {
            var rover = new Rover(Direction.East, 0, 0, _mockRoverEngine, _mockForwardNavigator);

            rover.Move("f");

            _mockRoverEngine.VerifyAllExpectations();
            _mockForwardNavigator.VerifyAllExpectations();
        }

        [TestMethod]
        public void And_the_rover_facing_south_attempts_to_move_forward_then_it_uses_the_navigator_to_see_if_the_rover_can_move()
        {
            var rover = new Rover(Direction.South, 0, 0, _mockRoverEngine, _mockForwardNavigator);

            rover.Move("f");

            _mockRoverEngine.VerifyAllExpectations();
            _mockForwardNavigator.VerifyAllExpectations();
        }

        [TestMethod]
        public void And_the_rover_facing_west_attempts_to_move_forward_then_it_uses_the_navigator_to_see_if_the_rover_can_move()
        {
            var rover = new Rover(Direction.West, 0, 0, _mockRoverEngine, _mockForwardNavigator);

            rover.Move("f");

            _mockRoverEngine.VerifyAllExpectations();
            _mockForwardNavigator.VerifyAllExpectations();
        }

        [TestMethod]
        public void And_the_rover_facing_north_attempts_to_move_backward_then_it_uses_the_navigator_to_see_if_the_rover_can_move()
        {;
            var rover = new Rover(Direction.North, 0, 0, _mockRoverEngine, _mockBackwardNavigator);

            rover.Move("b");

            _mockRoverEngine.VerifyAllExpectations();
            _mockBackwardNavigator.VerifyAllExpectations();
        }

        [TestMethod]
        public void And_the_rover_facing_south_attempts_to_move_backward_then_it_uses_the_navigator_to_see_if_the_rover_can_move()
        {
            var rover = new Rover(Direction.South, 0, 0, _mockRoverEngine, _mockBackwardNavigator);

            rover.Move("b");

            _mockRoverEngine.VerifyAllExpectations();
            _mockBackwardNavigator.VerifyAllExpectations();
        }

        [TestMethod]
        public void And_the_rover_facing_east_attempts_to_move_backward_then_it_uses_the_navigator_to_see_if_the_rover_can_move()
        {
            var rover = new Rover(Direction.East, 0, 0, _mockRoverEngine, _mockBackwardNavigator);

            rover.Move("b");

            _mockRoverEngine.VerifyAllExpectations();
            _mockBackwardNavigator.VerifyAllExpectations();
        }

        [TestMethod]
        public void And_the_rover_facing_West_attempts_to_move_backward_then_it_uses_the_navigator_to_see_if_the_rover_can_move()
        {
            var rover = new Rover(Direction.West, 0, 0, _mockRoverEngine, _mockBackwardNavigator);

            rover.Move("b");

            _mockRoverEngine.VerifyAllExpectations();
            _mockBackwardNavigator.VerifyAllExpectations();
        }
    }
    // ReSharper restore InconsistentNaming
}
