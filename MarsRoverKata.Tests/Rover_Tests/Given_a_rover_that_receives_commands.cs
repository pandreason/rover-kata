﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace MarsRoverKata.Tests.Rover_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class Given_a_rover_that_receives_commands
    {
        private const int GridSize = 10;
        private static Rover _rover;

        [TestInitialize]
        public void TestInitialize()
        {
            _rover = new Rover(Direction.North, 0, 0, new RoverEngine(), new Navigator(GridSize, GridSize, new List<Coordinate>()));
        }

        [TestMethod]
        public void When_starting_at_0_0_facing_north_and_given_FFRFF_then_the_rover_is_at_2_2_facing_east()
        {
            _rover.Move("FFRFF");

            Assert.AreEqual(new Coordinate(2, 2), _rover.Position);
            Assert.AreEqual(Direction.East, _rover.Direction);
        }

        [TestMethod]
        public void And_there_is_a_lowercase_character_to_move_forward_then_the_rover_moves_correctly()
        {
            _rover.Move("f");

            Assert.AreEqual(new Coordinate(0, 1), _rover.Position);
            Assert.AreEqual(Direction.North, _rover.Direction);
        }

        [TestMethod]
        public void And_there_is_a_lowercase_character_to_move_backward_then_the_rover_moves_correctly()
        {
            _rover.Move("b");

            Assert.AreEqual(new Coordinate(0, GridSize - 1), _rover.Position);
            Assert.AreEqual(Direction.North, _rover.Direction);
        }

        [TestMethod]
        public void And_there_is_a_lowercase_character_to_turn_right_then_the_rover_turns_correctly()
        {
            _rover.Move("r");

            Assert.AreEqual(new Coordinate(0, 0), _rover.Position);
            Assert.AreEqual(Direction.East, _rover.Direction);
        }

        [TestMethod]
        public void And_there_is_a_lowercase_character_to_turn_left_then_the_rover_turns_correctly()
        {
            _rover.Move("l");

            Assert.AreEqual(new Coordinate(0, 0), _rover.Position);
            Assert.AreEqual(Direction.West, _rover.Direction);
        }
    }
    // ReSharper restore InconsistentNaming
}
